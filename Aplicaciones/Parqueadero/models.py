from django.db import models

# Create your models here.

class Tipo(models.Model):
    idTipAALM=models.AutoField(primary_key=True)
    nombreTipAALM=models.CharField(max_length=50)
    descripcionTipAALM=models.CharField(max_length=50)


class Cliente(models.Model):
    idCliAALM=models.AutoField(primary_key=True)
    nombreCliAALM=models.CharField(max_length=50)
    apellidoCliAALM=models.CharField(max_length=50)
    direccionCliAALM=models.CharField(max_length=50)
    telefonoCliAALM=models.CharField(max_length=50)
    fotografia=models.FileField(upload_to='clientes', null=True,blank=True)
    tipo=models.ForeignKey(Tipo,null=True,blank=True,on_delete=models.PROTECT)



class Marca(models.Model):
    idMarAALM=models.AutoField(primary_key=True)
    nombreMarAALM=models.CharField(max_length=50)
    descripcionMarAALM=models.CharField(max_length=50)
    fotografia=models.FileField(upload_to='marcas', null=True,blank=True)

class Vehiculo(models.Model):
    idVehAALM=models.AutoField(primary_key=True)
    placaVehAALM=models.CharField(max_length=50)
    modeloVehAALM=models.CharField(max_length=50)
    anofabricacionVehAALM=models.CharField(max_length=50)
    colorVehAALM=models.CharField(max_length=50)
    fotografia=models.FileField(upload_to='vehiculos', null=True,blank=True)
    cliente=models.ForeignKey(Cliente,null=True,blank=True,on_delete=models.PROTECT)
    marca=models.ForeignKey(Marca,null=True,blank=True,on_delete=models.PROTECT)

class Lugar(models.Model):
    idLugAALM=models.AutoField(primary_key=True)
    nombreLugAALM=models.CharField(max_length=50)
    estadoLugAALM=models.CharField(max_length=50)
    descripcionLugAALM=models.CharField(max_length=50)

class Registro(models.Model):
    idRegAALM=models.AutoField(primary_key=True)
    fechaIngresoRegAALM=models.CharField(max_length=50)
    fechaSalidaRegAALM=models.CharField(max_length=50)
    horaIngresoRegAALM=models.CharField(max_length=50)
    horaSalidaRegAALM=models.CharField(max_length=50)
    emailRegAALM=models.CharField(max_length=50)
    total= models.CharField(max_length=50)
    vehiculo=models.ForeignKey(Vehiculo,null=True,blank=True,on_delete=models.PROTECT)
    lugar=models.ForeignKey(Lugar,null=True,blank=True,on_delete=models.PROTECT)
