# Generated by Django 5.0 on 2024-02-08 03:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Parqueadero', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tipo',
            old_name='empresarialTipAALM',
            new_name='descripcionTipAALM',
        ),
        migrations.RenameField(
            model_name='tipo',
            old_name='particularTipAALM',
            new_name='nombreTipAALM',
        ),
        migrations.RemoveField(
            model_name='tipo',
            name='vipTipAALM',
        ),
    ]
