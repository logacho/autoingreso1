from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Tipo)
admin.site.register(Cliente)
admin.site.register(Vehiculo)
admin.site.register(Marca)
admin.site.register(Lugar)
admin.site.register(Registro)
