from django.shortcuts import render, redirect
from .models import *
from django.contrib import messages

from django.conf import settings

from django.http import HttpResponseRedirect
from django.core.mail import send_mail


# Create your views here.

def plantilla(request):
    return render(request,'plantilla.html')

def listadoTipos(request):
    tiposBdd=Tipo.objects.all()
    return render(request,'tipo.html',{'tipos':tiposBdd})

def guardarTipo(request):
    nombreTipAALM=request.POST["nombreTipAALM"]
    descripcionTipAALM=request.POST["descripcionTipAALM"]

    nuevoTipo=Tipo.objects.create(nombreTipAALM=nombreTipAALM, descripcionTipAALM=descripcionTipAALM)

    messages.success(request, 'Tipo Guardado exitosamente')
    return redirect('/listadoTipos/')

def eliminarTipo(request,idTipAALM):
    tipoEliminar=Tipo.objects.get(idTipAALM=idTipAALM)
    tipoEliminar.delete()
    messages.success(request, 'Tipo Eliminado exitosamente')
    return redirect('/listadoTipos/')

def editarTipo(request,idTipAALM):
    tipoEditar=Tipo.objects.get(idTipAALM=idTipAALM)
    return render(request,
    'editarTipo.html',{'tipo':tipoEditar})

def procesarActualizacionTipo(request):
    idTipAALM=request.POST["idTipAALM"]
    nombreTipAALM=request.POST["nombreTipAALM"]
    descripcionTipAALM=request.POST["descripcionTipAALM"]
    tipoEditar=Tipo.objects.get(idTipAALM=idTipAALM)
    tipoEditar.nombreTipAALM=nombreTipAALM
    tipoEditar.descripcionTipAALM=descripcionTipAALM

    tipoEditar.save()
    messages.success(request,'Tipo ACTUALIZADO Exitosamente')
    return redirect('/listadoTipos/')

def listadoClientes(request):
    clientesBdd=Cliente.objects.all()
    tiposBdd=Tipo.objects.all()
    return render(request,'cliente.html',{'clientes':clientesBdd, 'tipos':tiposBdd, 'navbar': 'cliente'})

def guardarCliente(request):
    id_tip=request.POST["id_tip"]
    tipoSeleccionado=Tipo.objects.get(idTipAALM=id_tip)
    nombreCliAALM=request.POST["nombreCliAALM"]
    apellidoCliAALM=request.POST["apellidoCliAALM"]
    direccionCliAALM=request.POST["direccionCliAALM"]
    telefonoCliAALM=request.POST["telefonoCliAALM"]
    fotografia=request.FILES.get("fotografia")

    nuevoCliente=Cliente.objects.create(nombreCliAALM=nombreCliAALM, apellidoCliAALM=apellidoCliAALM, direccionCliAALM=direccionCliAALM,
    telefonoCliAALM=telefonoCliAALM, fotografia=fotografia, tipo=tipoSeleccionado)

    messages.success(request, 'Cliente Guardado exitosamente')
    return redirect('/listadoClientes/')

def eliminarCliente(request,idCliAALM):
    clienteEliminar=Cliente.objects.get(idCliAALM=idCliAALM)
    clienteEliminar.delete()
    messages.success(request, 'Cliente Eliminado exitosamente')
    return redirect('/listadoClientes/')

def editarCliente(request,idCliAALM):
    clienteEditar=Cliente.objects.get(idCliAALM=idCliAALM)
    tiposBdd=Tipo.objects.all()
    return render(request,
    'editarCliente.html',{'cliente':clienteEditar, 'tipos':tiposBdd})

def procesarActualizacionCliente(request):
    idCliAALM=request.POST["idCliAALM"]
    id_tip=request.POST["id_tip"]
    tipoSeleccionado=Tipo.objects.get(idTipAALM=id_tip)
    nombreCliAALM=request.POST["nombreCliAALM"]
    apellidoCliAALM=request.POST["apellidoCliAALM"]
    direccionCliAALM=request.POST["direccionCliAALM"]
    telefonoCliAALM=request.POST["telefonoCliAALM"]
    fotografia=request.FILES.get("fotografia")

    #Insertando datos mediante el ORM de DJANGO
    clienteEditar=Cliente.objects.get(idCliAALM=idCliAALM)
    clienteEditar.tipo=tipoSeleccionado
    clienteEditar.nombreCliAALM=nombreCliAALM
    clienteEditar.apellidoCliAALM=apellidoCliAALM
    clienteEditar.direccionCliAALM=direccionCliAALM
    clienteEditar.telefonoCliAALM=telefonoCliAALM
    if fotografia is not None:
        clienteEditar.fotografia=fotografia

    clienteEditar.save()
    messages.success(request,'Cliente ACTUALIZADO Exitosamente')
    return redirect('/listadoClientes/')

def listadoMarcas(request):
    marcasBdd=Marca.objects.all()
    return render(request,'marca.html',{'marcas':marcasBdd})

def guardarMarca(request):
    nombreMarAALM=request.POST["nombreMarAALM"]
    descripcionMarAALM=request.POST["descripcionMarAALM"]
    fotografia=request.FILES.get("fotografia")

    nuevoMarca=Marca.objects.create(nombreMarAALM=nombreMarAALM, descripcionMarAALM=descripcionMarAALM, fotografia=fotografia)

    messages.success(request, 'Marca Guardado exitosamente')
    return redirect('/listadoMarcas/')

def eliminarMarca(request,idMarAALM):
    marcaEliminar=Marca.objects.get(idMarAALM=idMarAALM)
    marcaEliminar.delete()
    messages.success(request, 'Marca Eliminado exitosamente')
    return redirect('/listadoMarcas/')

def editarMarca(request,idMarAALM):
    marcaEditar=Marca.objects.get(idMarAALM=idMarAALM)
    return render(request,
    'editarMarca.html',{'marca':marcaEditar})

def procesarActualizacionMarca(request):
    idMarAALM=request.POST["idMarAALM"]
    nombreMarAALM=request.POST["nombreMarAALM"]
    descripcionMarAALM=request.POST["descripcionMarAALM"]
    fotografia=request.FILES.get("fotografia")
    marcaEditar=Marca.objects.get(idMarAALM=idMarAALM)
    marcaEditar.nombreMarAALM=nombreMarAALM
    marcaEditar.descripcionMarAALM=descripcionMarAALM
    if fotografia is not None:
        marcaEditar.fotografia=fotografia
    marcaEditar.save()
    messages.success(request,'Marca ACTUALIZADO Exitosamente')
    return redirect('/listadoMarcas/')

def listadoLugares(request):
    lugaresBdd=Lugar.objects.all()
    return render(request,'lugar.html',{'lugares':lugaresBdd})

def guardarLugar(request):
    nombreLugAALM=request.POST["nombreLugAALM"]
    estadoLugAALM=request.POST["estadoLugAALM"]
    descripcionLugAALM=request.POST["descripcionLugAALM"]

    nuevoLugar=Lugar.objects.create(nombreLugAALM=nombreLugAALM, estadoLugAALM=estadoLugAALM, descripcionLugAALM=descripcionLugAALM)

    messages.success(request, 'Lugar Guardado exitosamente')
    return redirect('/listadoLugares/')

def eliminarLugar(request,idLugAALM):
    lugarEliminar=Lugar.objects.get(idLugAALM=idLugAALM)
    lugarEliminar.delete()
    messages.success(request, 'Lugar Eliminado exitosamente')
    return redirect('/listadoLugares/')

def editarLugar(request,idLugAALM):
    lugarEditar=Lugar.objects.get(idLugAALM=idLugAALM)
    return render(request,
    'editarLugar.html',{'lugar':lugarEditar})

def procesarActualizacionLugar(request):
    idLugAALM=request.POST["idLugAALM"]
    nombreLugAALM=request.POST["nombreLugAALM"]
    estadoLugAALM=request.POST["estadoLugAALM"]
    descripcionLugAALM=request.POST["descripcionLugAALM"]
    lugarEditar=Lugar.objects.get(idLugAALM=idLugAALM)
    lugarEditar.nombreLugAALM=nombreLugAALM
    lugarEditar.estadoLugAALM=estadoLugAALM
    lugarEditar.descripcionLugAALM=descripcionLugAALM

    lugarEditar.save()
    messages.success(request,'Lugar ACTUALIZADO Exitosamente')
    return redirect('/listadoLugares/')


def listadoVehiculos(request):
    vehiculosBdd=Vehiculo.objects.all()
    clientesBdd=Cliente.objects.all()
    marcasBdd=Marca.objects.all()
    return render(request,'vehiculo.html',{'vehiculos':vehiculosBdd, 'clientes':clientesBdd, 'marcas':marcasBdd, 'navbar': 'vehiculo'})

def guardarVehiculo(request):
    id_cli=request.POST["id_cli"]
    clienteSeleccionado=Cliente.objects.get(idCliAALM=id_cli)
    id_mar=request.POST["id_mar"]
    marcaSeleccionado=Marca.objects.get(idMarAALM=id_mar)
    placaVehAALM=request.POST["placaVehAALM"]
    modeloVehAALM=request.POST["modeloVehAALM"]
    anofabricacionVehAALM=request.POST["anofabricacionVehAALM"]
    colorVehAALM=request.POST["colorVehAALM"]
    fotografia=request.FILES.get("fotografia")

    nuevoVehiculo=Vehiculo.objects.create(placaVehAALM=placaVehAALM, modeloVehAALM=modeloVehAALM, anofabricacionVehAALM=anofabricacionVehAALM,
    colorVehAALM=colorVehAALM, fotografia=fotografia, cliente=clienteSeleccionado, marca=marcaSeleccionado)

    messages.success(request, 'Vehiculo Guardado exitosamente')
    return redirect('/listadoVehiculos/')

def eliminarVehiculo(request,idVehAALM):
    vehiculoEliminar=Vehiculo.objects.get(idVehAALM=idVehAALM)
    vehiculoEliminar.delete()
    messages.success(request, 'Vehiculo Eliminado exitosamente')
    return redirect('/listadoVehiculos/')

def editarVehiculo(request,idVehAALM):
    vehiculoEditar=Vehiculo.objects.get(idVehAALM=idVehAALM)
    clientesBdd=Cliente.objects.all()
    marcasBdd=Marca.objects.all()
    return render(request,
    'editarVehiculo.html',{'vehiculo':vehiculoEditar, 'clientes':clientesBdd, 'marcas':marcasBdd})

def procesarActualizacionVehiculo(request):
    idVehAALM=request.POST["idVehAALM"]
    id_cli=request.POST["id_cli"]
    clienteSeleccionado=Cliente.objects.get(idCliAALM=id_cli)
    id_mar=request.POST["id_mar"]
    marcaSeleccionado=Marca.objects.get(idMarAALM=id_mar)
    placaVehAALM=request.POST["placaVehAALM"]
    modeloVehAALM=request.POST["modeloVehAALM"]
    anofabricacionVehAALM=request.POST["anofabricacionVehAALM"]
    colorVehAALM=request.POST["colorVehAALM"]
    fotografia=request.FILES.get("fotografia")

    #Insertando datos mediante el ORM de DJANGO0
    vehiculoEditar=Vehiculo.objects.get(idVehAALM=idVehAALM)
    vehiculoEditar.cliente=clienteSeleccionado
    vehiculoEditar.marca=marcaSeleccionado
    vehiculoEditar.placaVehAALM=placaVehAALM
    vehiculoEditar.modeloVehAALM=modeloVehAALM
    vehiculoEditar.anofabricacionVehAALM=anofabricacionVehAALM
    vehiculoEditar.colorVehAALM=colorVehAALM
    if fotografia is not None:
        vehiculoEditar.fotografia=fotografia


    vehiculoEditar.save()
    messages.success(request,
      'Vehiculo ACTUALIZADO Exitosamente')
    return redirect('/listadoVehiculos/')

def listadoRegistros(request):
    registrosBdd=Registro.objects.all()
    vehiculosBdd=Vehiculo.objects.all()
    lugaresBdd=Lugar.objects.all()
    return render(request,'registro.html',{'registros':registrosBdd, 'vehiculos':vehiculosBdd, 'lugares':lugaresBdd, 'navbar': 'registro'})

def guardarRegistro(request):
    id_veh=request.POST["id_veh"]
    vehiculoSeleccionado=Vehiculo.objects.get(idVehAALM=id_veh)
    id_lug=request.POST["id_lug"]
    lugarSeleccionado=Lugar.objects.get(idLugAALM=id_lug)
    fechaIngresoRegAALM=request.POST["fechaIngresoRegAALM"]
    fechaSalidaRegAALM=request.POST["fechaSalidaRegAALM"]
    horaIngresoRegAALM=request.POST["horaIngresoRegAALM"]
    horaSalidaRegAALM=request.POST["horaSalidaRegAALM"]
    emailRegAALM=request.POST["emailRegAALM"]
    total=request.POST["total"]


    nuevoRegistro=Registro.objects.create(fechaIngresoRegAALM=fechaIngresoRegAALM, fechaSalidaRegAALM=fechaSalidaRegAALM, horaIngresoRegAALM=horaIngresoRegAALM,
    horaSalidaRegAALM=horaSalidaRegAALM, emailRegAALM=emailRegAALM, total=total, vehiculo=vehiculoSeleccionado, lugar=lugarSeleccionado)


    messages.success(request, 'Registro Guardado exitosamente')
    return redirect('/listadoRegistros/')

def eliminarRegistro(request,idRegAALM):
    registroEliminar=Registro.objects.get(idRegAALM=idRegAALM)
    registroEliminar.delete()
    messages.success(request, 'Registro Eliminado exitosamente')
    return redirect('/listadoRegistros/')

def editarRegistro(request,idRegAALM):
    registroEditar=Registro.objects.get(idRegAALM=idRegAALM)
    vehiculosBdd=Vehiculo.objects.all()
    lugaresBdd=Lugar.objects.all()
    return render(request,
    'editarRegistro.html',{'registro':registroEditar, 'vehiculos':vehiculosBdd, 'lugares':lugaresBdd})

def procesarActualizacionRegistro(request):
    idRegAALM=request.POST["idRegAALM"]
    id_veh=request.POST["id_veh"]
    vehiculoSeleccionado=Vehiculo.objects.get(idVehAALM=id_veh)
    id_lug=request.POST["id_lug"]
    lugarSeleccionado=Lugar.objects.get(idLugAALM=id_lug)
    fechaIngresoRegAALM=request.POST["fechaIngresoRegAALM"]
    fechaSalidaRegAALM=request.POST["fechaSalidaRegAALM"]
    horaIngresoRegAALM=request.POST["horaIngresoRegAALM"]
    horaSalidaRegAALM=request.POST["horaSalidaRegAALM"]
    emailRegAALM=request.POST["emailRegAALM"]
    total=request.POST["total"]


    #Insertando datos mediante el ORM de DJANGO
    registroEditar=Registro.objects.get(idRegAALM=idRegAALM)
    registroEditar.vehiculo=vehiculoSeleccionado
    registroEditar.lugar=lugarSeleccionado
    registroEditar.fechaIngresoRegAALM=fechaIngresoRegAALM
    registroEditar.fechaSalidaRegAALM=fechaSalidaRegAALM
    registroEditar.horaIngresoRegAALM=horaIngresoRegAALM
    registroEditar.horaSalidaRegAALM=horaSalidaRegAALM
    registroEditar.emailRegAALM=emailRegAALM
    registroEditar.total=total


    registroEditar.save()
    messages.success(request,
      'Registro ACTUALIZADO Exitosamente')
    return redirect('/listadoRegistros/')


def vista1(request):
    return render(request, 'enviar_correo.html')

def enviar_correo(request):
    if request.method == 'POST':
        destinatario = request.POST.get('destinatario')
        asunto = request.POST.get('asunto')
        cuerpo = request.POST.get('cuerpo')
        send_mail(asunto, cuerpo, settings.EMAIL_HOST_USER, [destinatario], fail_silently=False)

        messages.success(request, 'Se ha enviado tu correo')
        return HttpResponseRedirect('/vista1')

    return render(request, 'enviar_correo.html')
