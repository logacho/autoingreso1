from django.urls import path
from . import views

urlpatterns=[
    path('',views.plantilla),

    path('listadoTipos/', views.listadoTipos, name='tipo'),
    path('guardarTipo/', views.guardarTipo),
    path('eliminarTipo/<idTipAALM>', views.eliminarTipo),
    path('editarTipo/<idTipAALM>', views.editarTipo),
    path('procesarActualizacionTipo/', views.procesarActualizacionTipo),

    path('listadoClientes/', views.listadoClientes, name='cliente'),
    path('guardarCliente/', views.guardarCliente),
    path('eliminarCliente/<idCliAALM>', views.eliminarCliente),
    path('editarCliente/<idCliAALM>', views.editarCliente),
    path('procesarActualizacionCliente/', views.procesarActualizacionCliente),

    path('listadoMarcas/', views.listadoMarcas, name='marca'),
    path('guardarMarca/', views.guardarMarca),
    path('eliminarMarca/<idMarAALM>', views.eliminarMarca),
    path('editarMarca/<idMarAALM>', views.editarMarca),
    path('procesarActualizacionMarca/', views.procesarActualizacionMarca),

    path('listadoLugares/', views.listadoLugares, name='lugar'),
    path('guardarLugar/', views.guardarLugar),
    path('eliminarLugar/<idLugAALM>', views.eliminarLugar),
    path('editarLugar/<idLugAALM>', views.editarLugar),
    path('procesarActualizacionLugar/', views.procesarActualizacionLugar),

    path('listadoVehiculos/', views.listadoVehiculos, name='vehiculo'),
    path('guardarVehiculo/', views.guardarVehiculo),
    path('eliminarVehiculo/<idVehAALM>', views.eliminarVehiculo),
    path('editarVehiculo/<idVehAALM>', views.editarVehiculo),
    path('procesarActualizacionVehiculo/', views.procesarActualizacionVehiculo),

    path('listadoRegistros/', views.listadoRegistros, name='registro'),
    path('guardarRegistro/', views.guardarRegistro),
    path('eliminarRegistro/<idRegAALM>', views.eliminarRegistro),
    path('editarRegistro/<idRegAALM>', views.editarRegistro),
    path('procesarActualizacionRegistro/', views.procesarActualizacionRegistro),

    path('enviar_correo/',views.enviar_correo, name='enviar_correo'),
    path('vista1/', views.vista1, name= 'vista1'),

]
